<?php
/**
 * @file
 * Some drush helpers.
 */

/**
 * For cache handling.
 */
require_once __DIR__ . '/devcontrol.cache.inc';

/**
 * Implements hook_drush_command().
 */
function devcontrol_drush_command() {
  return [
    'dev-cache-list' => [
      'description' => "List all caches that can be cleared",
      'aliases' => ['dcl'],
      'drupal dependencies' => [],
    ],
    'dev-cache-clear' => [
      'description' => "More granular way of clearing caches",
      'arguments' => [
        '...' => 'Caches to clear: ' . implode(', ', array_keys(devcontrol_cache_clear_info())),
      ],
      'aliases' => ['dcc'],
      'drupal dependencies' => [],
    ],
  ];
}

/**
 * Clear caches.
 */
function drush_devcontrol_dev_cache_list() {
  $rows = [];
  $caches = devcontrol_cache_clear_info();
  ksort($caches);
  foreach ($caches as $target => $info) {
    $rows[] = [$target, $info['title']];
  }
  drush_print_table($rows);
}

/**
 * Clear caches.
 */
function drush_devcontrol_dev_cache_clear() {

  $caches   = devcontrol_cache_clear_info();
  $targets  = func_get_args();

  foreach ($targets as $index => $target) {
    if (empty($caches[$target])) {
      drush_print(dt(" ! '@cache' does not exists, skipping", ['@cache' => $target]));
      unset($targets[$index]);
    }
  }

  foreach ($targets as $target) {
    $message = call_user_func($caches[$target]['callback']);
    if ($message) {
      drush_print(dt(" + '@cache': @message", ['@cache' => $target, '@message' => $message]));
    } else {
      drush_print(dt(" + '@cache': Cleared", ['@cache' => $target]));
    }
  }
}
